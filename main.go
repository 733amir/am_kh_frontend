package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	api := NewAPI()

	log.Print("listening ...")
	if err := http.ListenAndServeTLS("0.0.0.0:443", os.Getenv("CERT_FILE"), os.Getenv("KEY_FILE"), &api.Mux); err != nil {
		log.Fatalf("listen and serve: %v", err)
	}
}
