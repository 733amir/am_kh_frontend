package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"net/http"
)

const (
	TemplateSignup = iota
	TemplateLogin
	TemplateForgetPassword
	TemplateResetPassword
	TemplateProfile
	TemplateProfileEdit
)

type API struct {
	Mux       http.ServeMux
	templates map[int]*template.Template
}

func NewAPI() *API {
	api := new(API)

	api.templates = map[int]*template.Template{
		TemplateSignup:         template.Must(template.ParseFiles("templates/signup.html")),
		TemplateLogin:          template.Must(template.ParseFiles("templates/login.html")),
		TemplateForgetPassword: template.Must(template.ParseFiles("templates/forget.html")),
		TemplateResetPassword:  template.Must(template.ParseFiles("templates/reset.html")),
		TemplateProfile:        template.Must(template.ParseFiles("templates/profile.html")),
		TemplateProfileEdit:    template.Must(template.ParseFiles("templates/profile-edit.html")),
	}

	api.Mux.HandleFunc("/signup", api.signup)
	api.Mux.HandleFunc("/login", api.login)
	api.Mux.HandleFunc("/logout", api.logout)
	api.Mux.HandleFunc("/forgot-password", api.forgetPassword)
	api.Mux.HandleFunc("/profile", api.profile)
	api.Mux.HandleFunc("/profile/edit", api.profileEdit)
	api.Mux.HandleFunc("/profile/reset-password/", api.resetPassword)

	return api
}

type signUpRequestData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type signUpResponseData struct {
	Token string `json:"token"`
}

type signUpTemplateData struct {
	Error string
}

func (api *API) signup(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost && r.Method != http.MethodGet {
		http.Error(w, "only GET,POST http methods are allowed", http.StatusMethodNotAllowed)
		return
	}

	var templateData signUpTemplateData
	if r.Method == http.MethodPost {
		token := func() string {
			email := r.PostFormValue("email")
			pass1 := r.PostFormValue("password_1")
			pass2 := r.PostFormValue("password_2")
			idToken := r.PostFormValue("id_token")
			if pass1 != pass2 {
				templateData.Error = "passwords should match"
				return ""
			}

			data := map[string]interface{}{
				"email":    email,
				"password": pass1,
			}
			if idToken != "" {
				data = map[string]interface{}{
					"id_token": idToken,
				}
			}
			b, err := json.Marshal(data)
			if err != nil {
				log.Printf("sign up: marshal: %v", err)
				templateData.Error = "internal error"
				return ""
			}

			response, err := http.DefaultClient.Post(
				"http://localhost:8080/signup",
				"application/json",
				strings.NewReader(string(b)),
			)
			if err != nil {
				log.Printf("sign up: post: %v", err)
				templateData.Error = "internal error"
				return ""
			}
			defer response.Body.Close()

			if response.StatusCode != http.StatusCreated {
				body, err := ioutil.ReadAll(response.Body)
				if err != nil {
					log.Printf("sign up: read body: %v", err)
					templateData.Error = "internal error"
					return ""
				}
				log.Printf("sign up: bad status: %d - %s", response.StatusCode, string(body))
				if response.StatusCode/100 == 4 {
					templateData.Error = string(body)
				} else {
					templateData.Error = "internal error"
				}
				return ""
			}

			var responseData signUpResponseData
			if err := json.NewDecoder(response.Body).Decode(&responseData); err != nil {
				log.Printf("sign up: decode body: %v", err)
				templateData.Error = "internal error"
				return ""
			}

			return responseData.Token
		}()

		if token != "" {
			cookie := http.Cookie{
				Name:  "amkh_authorization",
				Value: token,
				Path:  "/",
			}

			http.SetCookie(w, &cookie)
			http.Redirect(w, r, "/profile/edit", http.StatusSeeOther)
			return
		}
	}

	err := api.templates[TemplateSignup].Execute(w, templateData)
	if err != nil {
		log.Printf("template: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
	}
}

type loginRequestData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type loginResponseData struct {
	Token string `json:"token"`
}

type loginTemplateData struct {
	Error string
}

func (api *API) login(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost && r.Method != http.MethodGet {
		http.Error(w, "only GET,POST http methods are allowed", http.StatusMethodNotAllowed)
		return
	}

	var templateData loginTemplateData
	if r.Method == http.MethodPost {
		token := func() string {
			email := r.PostFormValue("email")
			password := r.PostFormValue("password")
			idToken := r.PostFormValue("id_token")

			data := map[string]interface{}{
				"email":    email,
				"password": password,
			}
			if idToken != "" {
				data = map[string]interface{}{
					"id_token": idToken,
				}
			}
			b, err := json.Marshal(data)
			if err != nil {
				log.Printf("login: marshal: %v", err)
				templateData.Error = "internal error"
				return ""
			}

			response, err := http.DefaultClient.Post(
				"http://localhost:8080/login",
				"application/json",
				strings.NewReader(string(b)),
			)
			if err != nil {
				log.Printf("login: post: %v", err)
				templateData.Error = "internal error"
				return ""
			}
			defer response.Body.Close()

			if response.StatusCode == http.StatusNotFound {
				templateData.Error = "username does not exists"
				return ""
			}
			if response.StatusCode == http.StatusUnauthorized {
				templateData.Error = "password is wrong"
				return ""
			}
			if response.StatusCode != http.StatusCreated {
				body, err := ioutil.ReadAll(response.Body)
				if err != nil {
					log.Printf("login: read body: %v", err)
					templateData.Error = "internal error"
					return ""
				}
				log.Printf("login: bad status: %d - %s", response.StatusCode, string(body))
				templateData.Error = "internal error"
				return ""
			}

			var responseData loginResponseData
			if err := json.NewDecoder(response.Body).Decode(&responseData); err != nil {
				log.Printf("login: decode body: %v", err)
				templateData.Error = "internal error"
				return ""
			}

			return responseData.Token
		}()

		if token != "" {
			cookie := http.Cookie{
				Name:  "amkh_authorization",
				Value: token,
				Path:  "/",
			}

			http.SetCookie(w, &cookie)
			http.Redirect(w, r, "/profile", http.StatusSeeOther)
			return
		}
	}

	err := api.templates[TemplateLogin].Execute(w, templateData)
	if err != nil {
		log.Printf("template: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
	}
}

func (api *API) logout(w http.ResponseWriter, r *http.Request) {
	func() {
		token, err := r.Cookie("amkh_authorization")
		if err == http.ErrNoCookie {
			return
		} else if err != nil {
			log.Printf("logout: create request: %v", err)
			return
		}

		request, err := http.NewRequest(
			http.MethodPost,
			"http://localhost:8080/logout",
			nil,
		)
		if err != nil {
			log.Printf("logout: create request: %v", err)
			return
		}
		request.Header.Set("Content-Type", "application/json")
		request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token.Value))

		response, err := http.DefaultClient.Do(request)
		if err != nil {
			log.Printf("logout: send request: %v", err)
			return
		}
		defer response.Body.Close()

		if response.StatusCode != http.StatusOK {
			body, err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Printf("logout: read body: %v", err)
				return
			}
			log.Printf("logout: bad status: %d - %s", response.StatusCode, string(body))
			return
		}
	}()

	cookie := http.Cookie{
		Name:    "amkh_authorization",
		Value:   "deleted",
		Path:    "/",
		Expires: time.Time{},
		MaxAge:  0,
	}

	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/login", http.StatusSeeOther)
	return
}

type forgetPasswordRequestData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type forgetPasswordResponseData struct {
	Token string `json:"token"`
}

type forgetPasswordTemplateData struct {
	Sent  bool
	Email string
	Error string
}

func (api *API) forgetPassword(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost && r.Method != http.MethodGet {
		http.Error(w, "only GET,POST http methods are allowed", http.StatusMethodNotAllowed)
		return
	}

	var templateData forgetPasswordTemplateData
	if r.Method == http.MethodPost {
		func() {
			email := r.PostFormValue("email")

			b, err := json.Marshal(map[string]interface{}{
				"email": email,
			})
			if err != nil {
				log.Printf("forget password: marshal: %v", err)
				templateData.Error = "internal error"
				return
			}

			response, err := http.DefaultClient.Post(
				"http://localhost:8080/profile/password/reset-link",
				"application/json",
				strings.NewReader(string(b)),
			)
			if err != nil {
				log.Printf("forget password: post: %v", err)
				templateData.Error = "internal error"
				return
			}
			defer response.Body.Close()

			if response.StatusCode == http.StatusNotFound {
				templateData.Error = "email does not exists"
				return
			}
			if response.StatusCode != http.StatusOK {
				body, err := ioutil.ReadAll(response.Body)
				if err != nil {
					log.Printf("forget password: read body: %v", err)
					templateData.Error = "internal error"
					return
				}
				log.Printf("forget password: bad status: %d - %s", response.StatusCode, string(body))
				templateData.Error = "internal error"
				return
			}

			templateData.Sent = true
			templateData.Email = email
		}()
	}

	err := api.templates[TemplateForgetPassword].Execute(w, templateData)
	if err != nil {
		log.Printf("template: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
	}
}

type resetPasswordRequestData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type resetPasswordResponseData struct {
	Token string `json:"token"`
}

type resetPasswordTemplateData struct {
	Token                  string
	Error                  string
	ShowForgetPasswordLink bool
}

func (api *API) resetPassword(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost && r.Method != http.MethodGet {
		http.Error(w, "only GET,POST http methods are allowed", http.StatusMethodNotAllowed)
		return
	}

	var templateData resetPasswordTemplateData
	templateData.Token = strings.TrimPrefix(r.URL.Path, "/profile/reset-password/")
	if r.Method == http.MethodPost {
		redirectToLogin := func() bool {
			token := r.PostFormValue("token")
			pass1 := r.PostFormValue("password_1")
			pass2 := r.PostFormValue("password_2")
			if pass1 != pass2 {
				templateData.Error = "passwords should match"
				return false
			}

			b, err := json.Marshal(map[string]interface{}{
				"new_password": pass1,
			})
			if err != nil {
				log.Printf("reset password: marshal: %v", err)
				templateData.Error = "internal error"
				return false
			}

			request, err := http.NewRequest(
				http.MethodPost,
				"http://localhost:8080/profile/password",
				strings.NewReader(string(b)),
			)
			if err != nil {
				log.Printf("reset password: create request: %v", err)
				templateData.Error = "internal error"
				return false
			}
			request.Header.Set("Content-Type", "application/json")
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

			response, err := http.DefaultClient.Do(request)
			if err != nil {
				log.Printf("reset password: send request: %v", err)
				templateData.Error = "internal error"
				return false
			}
			defer response.Body.Close()

			if response.StatusCode == http.StatusUnauthorized {
				templateData.Error = "link is expired"
				templateData.ShowForgetPasswordLink = true
				return false
			}
			if response.StatusCode != http.StatusOK {
				body, err := ioutil.ReadAll(response.Body)
				if err != nil {
					log.Printf("reset password: read body: %v", err)
					templateData.Error = "internal error"
					return false
				}
				log.Printf("reset password: bad status: %d - %s", response.StatusCode, string(body))
				if response.StatusCode/100 == 4 {
					templateData.Error = string(body)
				} else {
					templateData.Error = "internal error"
				}
				return false
			}

			return true
		}()

		if redirectToLogin {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}
	}

	err := api.templates[TemplateResetPassword].Execute(w, templateData)
	if err != nil {
		log.Printf("template: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
	}
}

type profileRequestData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type profileResponseData struct {
	Email     string `json:"email"`
	FullName  string `json:"fullname"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
}

type profileTemplateData struct {
	Email     string
	FullName  string
	Address   string
	Telephone string
	Error     string
}

func (api *API) profile(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only GET http method is allowed", http.StatusMethodNotAllowed)
		return
	}

	var templateData profileTemplateData

	redirectToLogin := func() bool {
		token, err := r.Cookie("amkh_authorization")
		if err == http.ErrNoCookie {
			return true
		} else if err != nil {
			log.Printf("profile: create request: %v", err)
			templateData.Error = "internal error"
			return false
		}

		request, err := http.NewRequest(
			http.MethodGet,
			"http://localhost:8080/profile",
			nil,
		)
		if err != nil {
			log.Printf("profile: create request: %v", err)
			templateData.Error = "internal error"
			return false
		}
		request.Header.Set("Content-Type", "application/json")
		request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token.Value))

		response, err := http.DefaultClient.Do(request)
		if err != nil {
			log.Printf("profile: send request: %v", err)
			templateData.Error = "internal error"
			return false
		}
		defer response.Body.Close()

		if response.StatusCode == http.StatusUnauthorized {
			return true
		}
		if response.StatusCode != http.StatusOK {
			body, err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Printf("profile: read body: %v", err)
				templateData.Error = "internal error"
				return false
			}
			log.Printf("profile: bad status: %d - %s", response.StatusCode, string(body))
			templateData.Error = "internal error"
			return false
		}

		var responseData profileResponseData
		if err := json.NewDecoder(response.Body).Decode(&responseData); err != nil {
			log.Printf("profile: decode body: %v", err)
			templateData.Error = "internal error"
			return false
		}

		templateData.Email = responseData.Email
		templateData.FullName = responseData.FullName
		templateData.Address = responseData.Address
		templateData.Telephone = responseData.Telephone
		return false
	}()

	if redirectToLogin {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	w.Header().Add("Pragma", "no-cache")
	w.Header().Add("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Add("Expires", "0 ")

	err := api.templates[TemplateProfile].Execute(w, templateData)
	if err != nil {
		log.Printf("template: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
	}
}

type profileEditRequestData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type profileEditResponseData struct {
	Email     string `json:"email"`
	FullName  string `json:"fullname"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
}

type profileEditTemplateData struct {
	Email     string
	FullName  string
	Address   string
	Telephone string
	Error     string
}

func (api *API) profileEdit(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost && r.Method != http.MethodGet {
		http.Error(w, "only GET,POST http methods are allowed", http.StatusMethodNotAllowed)
		return
	}

	w.Header().Add("Pragma", "no-cache")
	w.Header().Add("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Add("Expires", "0 ")

	if r.Method == http.MethodGet {
		var templateData profileEditTemplateData
		redirectToLogin := func() bool {
			token, err := r.Cookie("amkh_authorization")
			if err == http.ErrNoCookie {
				return true
			} else if err != nil {
				log.Printf("profile edit: create request: %v", err)
				templateData.Error = "internal error"
				return false
			}

			request, err := http.NewRequest(
				http.MethodGet,
				"http://localhost:8080/profile",
				nil,
			)
			if err != nil {
				log.Printf("profile edit: create request: %v", err)
				templateData.Error = "internal error"
				return false
			}
			request.Header.Set("Content-Type", "application/json")
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token.Value))

			response, err := http.DefaultClient.Do(request)
			if err != nil {
				log.Printf("profile edit: send request: %v", err)
				templateData.Error = "internal error"
				return false
			}
			defer response.Body.Close()

			if response.StatusCode == http.StatusUnauthorized {
				return true
			}
			if response.StatusCode != http.StatusOK {
				body, err := ioutil.ReadAll(response.Body)
				if err != nil {
					log.Printf("profile edit: read body: %v", err)
					templateData.Error = "internal error"
					return false
				}
				log.Printf("profile edit: bad status: %d - %s", response.StatusCode, string(body))
				templateData.Error = "internal error"
				return false
			}

			var responseData profileEditResponseData
			if err := json.NewDecoder(response.Body).Decode(&responseData); err != nil {
				log.Printf("profile edit: decode body: %v", err)
				templateData.Error = "internal error"
				return false
			}

			templateData.Email = responseData.Email
			templateData.FullName = responseData.FullName
			templateData.Address = responseData.Address
			templateData.Telephone = responseData.Telephone
			return false
		}()

		if redirectToLogin {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}

		err := api.templates[TemplateProfileEdit].Execute(w, templateData)
		if err != nil {
			log.Printf("template: %v", err)
			http.Error(w, "internal error", http.StatusInternalServerError)
		}
		return
	}

	if r.Method == http.MethodPost {
		var templateData profileEditTemplateData
		redirectToLogin, redirectToProfile := func() (bool, bool) {
			token, err := r.Cookie("amkh_authorization")
			if err == http.ErrNoCookie {
				return true, false
			} else if err != nil {
				log.Printf("profile edit: create request: %v", err)
				templateData.Error = "internal error"
				return false, false
			}

			b, err := json.Marshal(map[string]interface{}{
				"email":     r.PostFormValue("email"),
				"fullname":  r.PostFormValue("fullname"),
				"address":   r.PostFormValue("address"),
				"telephone": r.PostFormValue("telephone"),
			})

			request, err := http.NewRequest(
				http.MethodPost,
				"http://localhost:8080/profile",
				strings.NewReader(string(b)),
			)
			if err != nil {
				log.Printf("profile edit: create request: %v", err)
				templateData.Error = "internal error"
				return false, false
			}
			request.Header.Set("Content-Type", "application/json")
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token.Value))

			response, err := http.DefaultClient.Do(request)
			if err != nil {
				log.Printf("profile edit: send request: %v", err)
				templateData.Error = "internal error"
				return false, false
			}
			defer response.Body.Close()

			if response.StatusCode == http.StatusUnauthorized {
				return true, false
			}
			if response.StatusCode != http.StatusOK {
				body, err := ioutil.ReadAll(response.Body)
				if err != nil {
					log.Printf("profile edit: read body: %v", err)
					templateData.Error = "internal error"
					return false, false
				}
				log.Printf("profile edit: bad status: %d - %s", response.StatusCode, string(body))
				templateData.Error = "internal error"
				return false, false
			}

			var responseData profileEditResponseData
			if err := json.NewDecoder(response.Body).Decode(&responseData); err != nil {
				log.Printf("profile edit: decode body: %v", err)
				templateData.Error = "internal error"
				return false, false
			}

			return false, true
		}()

		if redirectToLogin {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
			return
		}

		if redirectToProfile {
			http.Redirect(w, r, "/profile", http.StatusSeeOther)
			return
		}

		err := api.templates[TemplateProfileEdit].Execute(w, templateData)
		if err != nil {
			log.Printf("template: %v", err)
			http.Error(w, "internal error", http.StatusInternalServerError)
		}
	}
}
